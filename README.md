# ecCodes #

There are 2 sources for the ecCodes package:

1. The official releases on the ECMWF website
2. The GitHub releases or code

An additional package (ecbuild) is required to build ecCodes. It is included in the ECMWF release, but must be added when getting the source code from GitHub. In the later case, it must be cloned (git clone https://github.com/ecmwf/ecbuild), patched (see patch fix-ecbuild_configure_file.patch), and added to the environment variables (ecbuild_DIR = D:\Development\_Conan\ecbuild\cmake).

## One step package creation

      conan create . terranum-conan+eccodes/stable

## Upload package to gitlab

      conan upload eccodes/2.27.0@terranum-conan+eccodes/stable --remote=gitlab --all

## Build the debug package

      conan create . terranum-conan+eccodes/stable -s build_type=Debug

Don't upload debug package on Gitlab.

## Step by step package creation

1. Get source code

        conan source . --source-folder=_bin
2. Create install files

        conan install . --install-folder=_bin --build=missing
3. Build

        conan build . --source-folder=_bin --build-folder=_bin
4. Create package 

        conan package . --source-folder=_bin --build-folder=_bin --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+eccodes/stable --source-folder=_bin --build-folder=_bin
6. Test package
      
        conan test test_package eccodes/2.27.0@terranum-conan+eccodes/stable


        