from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import subprocess


class ecCodesConan(ConanFile):
    name = "eccodes"
    description = "ecCodes"
    topics = ("conan", "ecCodes")
    url = "https://gitlab.com/terranum-conan/eccodes"
    homepage = "https://confluence.ecmwf.int/display/ECC/ecCodes+Home"
    license = "Apache License, Version 2.0"
    version = "a87ae7e"

    generators = ["cmake", "cmake_find_package"]
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "enable_jpg": [True, False],
        "enable_fortran": [True, False],
        "enable_threads": [True, False],
        "enable_aec": [True, False],
        "use_bash_from_git": [True, False],
    }
    default_options = {
        "shared": False,
        "enable_jpg": True,
        "enable_fortran": False,
        "enable_threads": False,
        "enable_aec": True,
        "use_bash_from_git": True,
    }

    _source_subfolder = "source_subfolder"
    _build_subfolder = "build_subfolder"

    def requirements(self):
        if self.options.enable_jpg:
            self.requires("openjpeg/2.5.0")
            self.requires("jasper/2.0.33")
        if self.options.enable_aec:
            self.requires("libaec/1.0.6")

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], strip_root=True, destination=self._source_subfolder)

    def export_sources(self):
        self.copy("CMakeLists.txt")
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            self.copy(patch["patch_file"])

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions['BUILD_SHARED_LIBS'] = self.options.shared
        cmake.definitions['ENABLE_TESTS'] = 'OFF'
        cmake.definitions['ENABLE_EXTRA_TESTS'] = 'OFF'
        cmake.definitions['ENABLE_EXAMPLES'] = 'OFF'
        cmake.definitions['ENABLE_TOOLS'] = 'OFF'
        cmake.definitions['HAVE_BUILD_TOOLS'] = 'OFF'
        cmake.definitions['ENABLE_NETCDF'] = 'OFF'
        cmake.definitions['ENABLE_JPG'] = self.options.enable_jpg
        cmake.definitions['ENABLE_FORTRAN'] = self.options.enable_fortran
        cmake.definitions['ENABLE_ECCODES_THREADS'] = self.options.enable_threads
        cmake.definitions['ENABLE_AEC'] = self.options.enable_aec
        if self.settings.os == 'Windows':
            if self.options.use_bash_from_git:
                cmake.definitions['BASH_EXE'] = "C:\\Program Files\\Git\\bin\\bash.exe"

        cmake.configure(build_folder=self._build_subfolder)

        return cmake

    def _patch_sources(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            tools.patch(**patch)

    def build(self):
        self._patch_sources()
        cmake = self._configure_cmake()
        cmake.build(target="eccodes")

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
